import logging
import math
import time
from datetime import datetime

from influxdb import InfluxDBClient

from home_auto_lib.master_clock import MasterClock
from home_auto_lib.mqtt import client
from home_auto_lib.value_objects import ExpirableData, TimestampedJSONObject

solar_topic = "home/sensors/solar/+"
solar_energy_topic = "home/sensors/solar/energy/+"

mains_topic = "home/sensors/mains/+"
power_topic = "home/sensors/power/+"

main_battery_topic = "home/sensors/main-battery/+"
multiplus_topic = "home/sensors/multiplus/#"
mppt2_topic = "home/sensors/solar/mppt2/#"
mppt3_topic = "home/sensors/solar/mppt3/#"

rad_topic = "home/sensors/radiation/+"


def on_mppt(mqtt_client, userdata, message):
    publish_mppt(userdata, message, "solar")


def on_multiplus(mqtt_client, userdata, message):
    publish_multiplus(userdata, message, "multiplus")


def on_jkbms(mqtt_client, userdata, message):
    publish_jkbms(userdata, message, "main_battery")


def on_solar_message(mqtt_client, userdata, message):
    publish_solar(userdata, message, "solar")


def on_mains_message(mqtt_client, userdata, message):
    publish_mains(userdata, message, "mains")


def on_power_message(mqtt_client, userdata, message):
    publish_power(userdata, message, "power")


def publish_solar(userdata, message, measurement):
    topic = message.topic
    payload = message.payload
    metric = str(topic).rpartition("/")[2]
    obj = ExpirableData.from_json(payload, "solar")
    data = obj.data
    timestamp = datetime.utcfromtimestamp(data["timestamp"])
    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": data["key"],
                "name": data["name"],
                "units": data["units"]
            },
            "time": timestamp,
            "fields": {
                "value": float(data["value"])
            }
        }
    ]
    write_json(json_body)


def publish_mains(userdata, message, measurement):
    topic = message.topic
    payload = message.payload
    topic = message.topic
    metric = str(topic).rpartition("/")[2]
    obj = ExpirableData.from_json(payload, "mains")
    data = obj.data
    if "timestamp" in data:
        timestamp = datetime.utcfromtimestamp(data["timestamp"])
    else:
        timestamp = master_clock.instance.get_current_time()
    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": data["key"],
                "name": data["name"],
                "units": data["units"]
            },
            "time": timestamp,
            "fields": {
                "value": float(data["value"])
            }
        }
    ]
    write_json(json_body)


def publish_power(userdata, message, measurement):
    topic = message.topic
    payload = message.payload
    topic = message.topic
    metric = str(topic).rpartition("/")[2]
    #print("Metric: {}".format(metric))
    obj = ExpirableData.from_json(payload, "power")
    data = obj.data
    timestamp = datetime.utcfromtimestamp(data["timestamp"])
    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": data["key"],
                "name": data["name"],
                "units": data["units"]
            },
            "time": timestamp,
            "fields": {
                "value": float(data["value"])
            }
        }
    ]
    write_json(json_body)


def write_json(json_body):
    logging.info("Write to influx: {0!s}".format(json_body))
    try:
        influx.write_points(json_body)  # , retention_policy="one_year")
    except Exception as e:
        logging.error("Error {0!s}".format(e))


def publish_jkbms(userdata, message, measurement):
    topic = message.topic
    payload = message.payload
    metric = str(topic).rpartition("/")[2]
    obj = TimestampedJSONObject.from_json(payload, "")
    data = obj.data
    timestamp = datetime.utcfromtimestamp(data["timestamp"])
    if data["units"] != "none":
        value = float(data["value"])
    else:
        return

    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": data["key"],
                "name": data["name"],
                "units": data["units"]
            },
            "time": timestamp,
            "fields": {
                "value": value
            }
        }
    ]
    write_json(json_body)


def publish_multiplus(userdata, message, measurement):
    topic = message.topic
    payload = message.payload
    obj = TimestampedJSONObject.from_json(payload, "")
    data = obj.data
    timestamp = datetime.utcfromtimestamp(data["timestamp"])
    if "/ACOut/" in topic:
        data["key"] = "ACOut_" + data["key"]
        data["name"] = "ACOut_" + data["name"]
    elif "/ACActiveIn/" in topic:
        data["key"] = "ACActiveIn_" + data["key"]
        data["name"] = "ACActiveIn_" + data["name"]
    elif "/Energy/" in topic:
        data["key"] = "Energy_" + data["key"]
        data["name"] = "Energy_" + data["name"]
    elif "/DC/" in topic:
        data["key"] = "DC_" + data["key"]
        data["name"] = "DC_" + data["name"]
    else:
        return
    value = float(data["value"])
    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": data["key"],
                "name": data["name"],
                "units": data["units"]
            },
            "time": timestamp,
            "fields": {
                "value": value
            }
        }
    ]
    write_json(json_body)


def publish_mppt(userdata, message, measurement):
    topic = message.topic
    payload = message.payload
    obj = TimestampedJSONObject.from_json(payload, "")
    data = obj.data
    zone = str(topic).rpartition("/")[2]

    timestamp = datetime.utcfromtimestamp(data["timestamp"])
    value = float(data["value"])

    if "mppt2" in message.topic:
        prefix = "mppt2_"
    elif "mppt3" in message.topic:
        prefix = "mppt3_"
    else:
        prefix = "uknown"

    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": prefix + data["key"],
                "name": prefix + data["name"],
                "units": data["units"]
            },
            "time": timestamp,
            "fields": {
                "value": value
            }
        }
    ]
    write_json(json_body)


def publish(userdata, message, measurement):
    topic = message.topic
    payload = message.payload
    zone = str(topic).rpartition("/")[2]
    obj = ExpirableData.from_json(payload, zone)
    data = obj.data
    timestamp = datetime.utcfromtimestamp(data["timestamp"])
    if math.isnan(float(data["value"])):
        logging.debug("NaN value received: {0!s}".format(data))
        return
    if measurement == "temperature" and (float(data["value"]) < -20 or float(data["value"]) > 84):
        logging.debug("OutOfWhack value received: {0!s}".format(data))
        return
    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": data["key"],
                "name": data["name"]
            },
            "time": timestamp,
            "fields": {
                "value": float(data["value"])
            }
        }
    ]
    write_json(json_body)


def on_rad(mqtt_client, userdata, message):
    try:
        obj = TimestampedJSONObject.from_json(message.payload, "")
        data = obj.data
        timestamp = datetime.utcfromtimestamp(data["timestamp"])
        json_body = [
            {
                "measurement": "radiation",
                "tags": {
                    "key": data["key"],
                    "name": data["name"]
                },
                "time": timestamp,
                "fields": {
                    "value": float(data["value"])
                }
            }
        ]
        write_json(json_body)
    except Exception as e:
        logging.error("Radiation update Failed: {0!s} - {1!s}".format(message.payload, e))
        pass


def on_mppt_tot(mqtt_client, userdata, message):
    try:
        obj = TimestampedJSONObject.from_json(message.payload, "")
        data = obj.data
        timestamp = datetime.utcfromtimestamp(data["timestamp"])
        json_body = [
            {
                "measurement": "solar",
                "tags": {
                    "key": data["key"],
                    "name": data["name"]
                },
                "time": timestamp,
                "fields": {
                    "value": float(data["value"])
                }
            }
        ]
        write_json(json_body)
    except Exception as e:
        logging.error("MPPT Total Failed: {0!s} - {1!s}".format(message.payload, e))
        pass


influx = InfluxDBClient("influxdb", 8087, 'root', 'root', 'home_auto', pool_size=100)

master_clock = MasterClock(client, "home/clock")
master_clock.subscribe_to_clock()
time.sleep(1)

client.subscribe(solar_topic)
client.subscribe(solar_energy_topic)
client.subscribe(mains_topic)
client.subscribe(power_topic)
client.subscribe(main_battery_topic)
client.subscribe(multiplus_topic)
client.subscribe(mppt2_topic)
client.subscribe(mppt3_topic)
client.subscribe(rad_topic)
client.subscribe("home/sensors/solar/mppt_total")
client.subscribe("home/sensors/solar/battery_power_limit")

client.message_callback_add(solar_topic, on_solar_message)
client.message_callback_add(solar_energy_topic, on_solar_message)
client.message_callback_add(mains_topic, on_mains_message)
client.message_callback_add(power_topic, on_power_message)
client.message_callback_add(main_battery_topic, on_jkbms)
client.message_callback_add(multiplus_topic, on_multiplus)
client.message_callback_add(mppt2_topic, on_mppt)
client.message_callback_add(mppt3_topic, on_mppt)
client.message_callback_add(rad_topic, on_rad)
client.message_callback_add("home/sensors/solar/mppt_total", on_mppt_tot)
client.message_callback_add("home/sensors/solar/battery_power_limit", on_mppt_tot)

client.blocking_run()
