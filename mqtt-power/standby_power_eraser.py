from home_auto_lib.mqtt_lib import MqttClient, MqttMultiTopicObservableCache, MqttTopicMatcher
from home_auto_lib.value_objects import TimestampedJSONObject
import logging

from home_auto_lib.mqtt import client, cache, get_clock

standby_limits = {
    "bedroom_media_power_power": {
        "threshold": 20,
        "timeout": 30
    },
    "livingroom_media_power_power": {
        "threshold": 30,
        "timeout": 30
    },
    "office_essbus_power_power": {
        "threshold": 10,
        "timeout": 60
    }
}
power_topic = "home/sensors/power/"
power_states_topic = "home/power_states/standby/"


class StandByPowerDetector:
    def __init__(self, mqtt_client: MqttClient, cache:  MqttMultiTopicObservableCache ):
        self.client = mqtt_client
        self.cache = cache

    def notify(self, topic):
        logging.debug("Notified about topic:{0!s}".format(topic))
        if MqttTopicMatcher.matches(topic, power_topic+"+"):
            self.handle_power(topic)
        elif MqttTopicMatcher.matches(topic, power_states_topic+"+"):
            self.handle_state(topic)
        else:
            return

    def handle_state(self, topic):
        state_payload = self.cache.last_for_topic(topic)
        if not state_payload or state_payload is not dict:
            logging.info("Power state cancelled for topic:{0!s}".format(topic))
            return

        topic_path = topic.split("/")
        device = topic_path[-1]
        state_json = TimestampedJSONObject.from_json(state_payload, "power")
        logging.debug("Payload for lot:{0!s} = {1!s}".format(topic, state_json))
        logging.debug("Received state message: {0!s}".format(state_payload))

    def handle_power(self, topic):
        power_payload = self.cache.last_for_topic(topic)
        if not power_payload or not isinstance(power_payload, dict):
            logging.warning("No cached payload for topic:{0!s}".format(topic))
            return

        topic_path = topic.split("/")
        device = topic_path[-1]
        if "_power" in device and device in standby_limits:
            logging.debug("{0!s} looks like a power meter".format(device))
        else:
            logging.debug("{0!s} not a power meter ignoring".format(device))
            return

        power_json = TimestampedJSONObject(power_payload, device)
        logging.debug("Payload for lot:{0!s} = {1!s}".format(topic, power_json))

        power_val = power_json.get_path("ENERGY.Power")
        #if not power_val:
        #    logging.warning("key {0!s} not found in json {1!s}".format("ENERGY.Power", power_json))
        #    return  # no action
        if not isinstance(power_val, (int, float)):
            logging.warning("Bogus power value {0!s}".format(power_val))
            return  # no action
        power_thres = standby_limits[device]["threshold"]
        if float(power_val) < power_thres:
            # in standby range
            power_state = self.cache.last_for_topic(power_states_topic+device)
            if power_state and isinstance(power_state, dict):
                return  # already notifiied.
                        # MULTI-CACHE CONCURRENCY WARNING!  REVIEW!
            else:
                # Publish standby state
                state = TimestampedJSONObject({}, device)
                state.set_timestamp(get_clock())
                client.publish( power_states_topic+device, state.to_json() )
        else:
            # Clear standby state
            client.publish(power_states_topic + device, None)


detector = StandByPowerDetector(client, cache)

cache.add_subscription_with_observer(power_topic+"+", detector)
cache.add_subscription_with_observer(power_states_topic+"+", detector)


client.blocking_run()
