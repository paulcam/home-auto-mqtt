from home_auto_lib.mqtt_lib import MqttClient, MqttMultiTopicObservableCache
import time
import json
import logging
from home_auto_lib.mqtt import client, cache, get_clock

max_current = 30
thres_voltage = 3.4


battery_max_cell_topic = "main-battery/sensor/main-battery_max_cell_voltage/state"
battery_cell_delta_topic = "main-battery/sensor/main-battery_delta_cell_voltage/state"
battery_power_topic = "main-battery/sensor/main-battery_power/state"
mppt2_charge_limit_topic = "venus-home/W/b827eb3e8f99/solarcharger/289/Settings/ChargeCurrentLimit"
mppt3_charge_limit_topic = "venus-home/W/b827eb3e8f99/solarcharger/290/Settings/ChargeCurrentLimit"
battery_power_limit_topic = "home/battery_net_charge"
TARGET_MAX_WATTS = 1000
MAX_BALANCE_WATTS=1000
MAX_DELTA=200
DELTA_COEF = float(MAX_BALANCE_WATTS) / MAX_DELTA
DELTA_AGGRESSION = 1
MAX_CELL=3.6

mppt1_power_topic = "solar/panel/P"
mppt2_power_topic = "venus-home/N/b827eb3e8f99/solarcharger/289/Yield/Power"
mppt3_power_topic = "venus-home/N/b827eb3e8f99/solarcharger/290/Yield/Power"
mppt_total_power_topic = "home/mppt_total"


# Utility service, publishs the MPPT_Total rather than trying to have grafana totalise graphs (its shit)
class MPPTTotalizer:
    # Note:  With 3 observers this controller will emit 3 times the number of 
    # message per minute, one for each mppt update, just so you know
    
    def __init__(self, client, cache):
        self.client = client
        self.cache = cache
        self.cache.add_subscription( mppt1_power_topic )
        self.cache.add_observer( mppt1_power_topic, self)
        self.cache.add_subscription( mppt2_power_topic )
        self.cache.add_observer( mppt2_power_topic, self)
        self.cache.add_subscription( mppt3_power_topic )
        self.cache.add_observer( mppt3_power_topic, self)

    def notify(self, topic):
        m1 = self.cache.last_for_topic(mppt1_power_topic)
        m2 = self.cache.last_for_topic(mppt2_power_topic)
        m3 = self.cache.last_for_topic(mppt3_power_topic)

        # Note mppt1 has a "value" field.  mppt2 n 3 are raw floats

        if not m1 or not m2 or not m3:
            return
        try:
            m2=m2["value"]
            m3=m3["value"]
            self.client.publish_retained(mppt_total_power_topic, m1+m2+m3 )
        except:
            pass


# Using battery cell voltages and delta determines the maximum battery charge wattage.
class NetBatteryChargeLimiter:
    def __init__(self, client, cache):
        self.client = client
        self.cache = cache
        self.cache.add_subscription( battery_max_cell_topic )
        self.cache.add_observer(battery_max_cell_topic, self)
        self.cache.add_subscription( battery_cell_delta_topic )

    def notify(self, topic):
        max_cell = self.cache.last_for_topic(battery_max_cell_topic)
        cell_delta = self.cache.last_for_topic(battery_cell_delta_topic)
        logging.debug("Mx: {0!s} Delta: {1!s} Delta_coef: {2!s}".format( max_cell, cell_delta, DELTA_COEF ))

        if not cell_delta or not max_cell:
            return

        if max_cell < thres_voltage:
            self.client.publish(battery_power_limit_topic, TARGET_MAX_WATTS )
            return

        if max_cell > MAX_CELL:
            self.client.publish(battery_power_limit_topic, 10 )
            return

        if cell_delta > 0.050:
            self.client.publish(battery_power_limit_topic, MAX_BALANCE_WATTS - (cell_delta * 1000 *DELTA_COEF * DELTA_AGGRESSION) )

# Compares the battery charge limit power to the present net battery charge.
# hunts/tracks by moving the mppt current limit up or down to approach the limit
# a (currently) 30 watt histeris "notch" is provided to prevent oscilations
# Draw backs include the mppt controllers not responding to a current change
# before the battery emits a new power value.  This unresponsive event produces a 
# repeat action and will cause overshoots and undershoots.
# On higher power spikes it can take quite a while to pull down the limit.
# presently 0.5A every 5 seconds.  From 14A down to 4A can take 100 seconds!
class NetBatteryChargeTracker:
    def __init__(self, client, cache):
        self.client = client
        self.cache = cache
        self.cache.add_subscription( battery_power_limit_topic )
        self.cache.add_observer(battery_power_limit_topic, self)
        self.cache.add_subscription( battery_power_topic )
        self.cache.add_observer(battery_power_topic, self)
        self.cache.add_subscription( mppt2_charge_limit_topic )
        self.cache.add_subscription( mppt3_charge_limit_topic )
        self.client.publish( mppt2_charge_limit_topic, json.dumps({"value": 6})  )
        self.client.publish( mppt3_charge_limit_topic, json.dumps({"value": 6})  )

    def notify(self, topic):
        power_limit = self.cache.last_for_topic(battery_power_limit_topic)
        power = self.cache.last_for_topic(battery_power_topic)
        mppt2_limit = self.cache.last_for_topic(mppt2_charge_limit_topic)
        mppt3_limit = self.cache.last_for_topic(mppt3_charge_limit_topic)


        logging.debug("PL: {0!s} P: {1!s} L1: {2!s} L2: {3!s}".format( power_limit, power, mppt2_limit, mppt3_limit ))
        if not power_limit or not power or not mppt2_limit or not mppt3_limit:
            return

        mppt2_limit_f = mppt2_limit
        mppt3_limit_f = mppt3_limit

        if isinstance(mppt2_limit, dict):
            mppt2_limit_f = float( mppt2_limit["value"] )
        else:
            mppt2_limif_f = mppt2_limit

        if isinstance(mppt3_limit, dict):
            mppt3_limit_f = float( mppt3_limit["value"] )
        else:
            mppt3_limit_f = mppt3_limit


        if power <= power_limit-15:
            self.increase_power(mppt2_limit_f, mppt3_limit_f)
        if power > power_limit+15:
            self.decrease_power(mppt2_limit_f, mppt3_limit_f)

    def increase_power(self, m2, m3):
        # Already max
        if m2 >= 14 and m3 >= 14:
            return

        if m2 <= 14:
            self.client.publish(mppt2_charge_limit_topic, json.dumps({"value": m2+0.25}))
        if m3 <= 14:
            self.client.publish(mppt3_charge_limit_topic, json.dumps({"value": m3+0.25}))

    def decrease_power(self, m2, m3):
        # Already min
        down_by = 0.25
        if m2 < down_by and m3 < down_by:
            return

        if m2 > down_by:
            self.client.publish(mppt2_charge_limit_topic, json.dumps({"value": m2-down_by}))
        if m3 > down_by:
            self.client.publish(mppt3_charge_limit_topic, json.dumps({"value": m3-down_by})) 



def on_clock(client, userdata, message):
    global clock
    clock_obj = json.loads(message.payload.decode("UTF-8"))
    if "utctime" in clock_obj:
        utctime = clock_obj["utctime"]
        clock = float(utctime)

mqtt = MqttClient(broker_address="mqtt", broker_port=1883, client_name="NetBatteryChargeLimiter")
cache = MqttMultiTopicObservableCache(mqtt, [], None)
clock = time.time()
mqtt.subscribe("home/clock")
mqtt.message_callback_add("home/clock", on_clock)
time.sleep(5)

cntlr = NetBatteryChargeLimiter(mqtt, cache)
cntlr2 = NetBatteryChargeTracker(mqtt, cache)
cntlr3 = MPPTTotalizer(mqtt, cache)

mqtt.blocking_run()
