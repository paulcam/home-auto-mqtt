import os
import logging
from home_auto_lib.random_strings import get_random_string
from home_auto_lib.mqtt_lib import MqttClient, MqttMultiTopicObservableCache

global SERVICE_NAME
global MQTT_BROKER_ADDR
global MQTT_BROKER_PORT
global client
global cache
global master_clock
global get_clock

logging.basicConfig(level=logging.DEBUG,
                    format="%(asctime)s %(levelname)s %(message)s [%(filename)s:%(lineno)d]")

if "SERVICE_NAME" in os.environ:
    SERVICE_NAME = os.environ["SERVICE_NAME"]
else:
    SERVICE_NAME = "PowerSrv-"+get_random_string(4)
    logging.warning("No ENV for SERVICE_NAME defaulting to random {0!s}".format(SERVICE_NAME))

if "MQTT_BROKER_ADDR" in os.environ:
    MQTT_BROKER_ADDR = os.environ["MQTT_BROKER_ADDR"]
else:
    logging.warning("No ENV for MQTT_BROKER_ADDR defaulting to mqtt")
    MQTT_BROKER_ADDR = "mqtt"

if "MQTT_BROKER_PORT" in os.environ:
    MQTT_BROKER_PORT = int(os.environ["MQTT_BROKER_PORT"])
else:
    logging.warning("No ENV for MQTT_BROKER_PORT defaulting to 1883")
    MQTT_BROKER_PORT = 1883

logging.info("Config: MQTT_BROKER_ADDR={0!s} MQTT_BROKER_PORT={1!s}".format(MQTT_BROKER_ADDR, MQTT_BROKER_PORT))
logging.debug("Connecting...")


client = MqttClient(broker_address=MQTT_BROKER_ADDR, broker_port=MQTT_BROKER_PORT, client_name=SERVICE_NAME)
cache = MqttMultiTopicObservableCache(client, [], None)

