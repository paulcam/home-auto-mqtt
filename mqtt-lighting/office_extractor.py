from home_auto_lib.mqtt_lib import MqttMultiTopicObservableCache
import logging
from home_auto_lib.mqtt import client, cache


class OfficeExtractorController:
    def __init__(self, mqtt_client, mqtt_cache: MqttMultiTopicObservableCache):
        self.client = mqtt_client
        self.cache = mqtt_cache
        self.office_pm_topic = "airgradient1/sensor/pm_10_0/state"
        self.office_co2_topic = "airgradient1/sensor/co2/state"
        self.extractor_cmd_topic = "cmnd/office-extractor/POWER"
        self.cache.add_subscription_with_observer(self.office_pm_topic, self)
        self.cache.add_subscription_with_observer(self.office_co2_topic, self)

    def notify(self, topic):
        pm_10_0_payload = self.cache.last_for_topic(self.office_pm_topic)
        co2_payload = self.cache.last_for_topic(self.office_co2_topic)

        required_state = False

        if pm_10_0_payload and pm_10_0_payload > 60:
            logging.debug("PM10.0: {0!s}".format(pm_10_0_payload))
            required_state = True

        if co2_payload and co2_payload > 800:
            logging.debug("CO2: {0!s}".format(co2_payload))
            required_state = True

        if required_state:
            self.turnon()
        else:
            self.turnoff()

    def turnon(self):
        payload = "ON"
        self.client.publish(self.extractor_cmd_topic, payload)

    def turnoff(self):
        payload = "OFF"
        self.client.publish(self.extractor_cmd_topic, payload)


cntlr = OfficeExtractorController(client, cache)

client.blocking_run()
