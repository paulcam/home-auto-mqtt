import datetime

from home_auto_lib.mqtt_lib import MqttClient
from home_auto_lib.value_objects import TimestampedJSONObject
from home_auto_lib.mqtt import client, internal
import time
import json
import re
import math

def on_clock(client, userdata, message):
    global clock
    clock_obj = json.loads(message.payload.decode("UTF-8"))
    if "utctime" in clock_obj:
        utctime = clock_obj["utctime"]
        clock = float(utctime)


def republish_client(client, userdata, message):
    pass
    client.publish(message.topic, message.payload)


def on_smartbulb(client, userdata, message):
    publish_client:MqttClient = userdata["internal"]
    bulb_key = message.topic.rpartition("/")[2]
    bulb_name = bulb_key.rpartition("_")[0]
    message_obj = json.loads(message.payload.decode("UTF-8"))
    json_out = TimestampedJSONObject(message_obj, "lighting")
    json_out.set_timestamp(time.time())
    json_out.data["key"] = bulb_key
    json_out.data["name"] = bulb_name
    publish_client.publish_retained("home/lighting/bulbs/{0!s}".format(bulb_name), json_out.to_json())


def on_battery(client, userdata, message, message_obj):
    publish_client: MqttClient = userdata["internal"]
    device_key = message.topic.rpartition("/")[2] + "_battery"
    json_out = TimestampedJSONObject(message_obj, None)
    json_out.set_timestamp(time.time())
    json_out.data["key"] = device_key
    json_out.data["name"] = device_key
    publish_client.publish_retained("home/zigbee-batteries/{0!s}".format(device_key), json_out.to_json())


def on_lux(client, userdata, message, message_obj, topic_prefix):
    publish_client: MqttClient = userdata["internal"]
    device_key = message.topic.rpartition("/")[2]
    json_out = TimestampedJSONObject(message_obj, None)
    json_out.set_timestamp(time.time())
    json_out.data["key"] = device_key
    json_out.data["name"] = device_key
    publish_client.publish_retained("{1!s}{0!s}".format(device_key,topic_prefix), json_out.to_json())


def on_zigbee(client, userdata, message):
    if message.topic:
        if "bulb" in message.topic or "overhead" in message.topic or "lamp" in message.topic:
            on_smartbulb(client, userdata, message)
        message_obj = json.loads(message.payload.decode("UTF-8"))
        if "battery" in message_obj:
            on_battery(client, userdata, message, message_obj)
        if "illuminance" in message_obj:
            on_lux(client, userdata, message, message_obj, "home/lighting/lux/")
        if "occupancy" in message_obj:
            on_lux(client, userdata, message, message_obj, "home/occupancy/")

def on_airgradient(client, userdata, message):
    publish_client: MqttClient = userdata["internal"]
    device_key = message.topic.rpartition("/")[0].rpartition("/")[2]
    json_out = TimestampedJSONObject(dict(), "office")
    json_out.set_timestamp(time.time())
    json_out.data["key"] = device_key
    json_out.data["name"] = device_key
    json_out.data["value"] = message.payload.decode()
    publish_client.publish_retained("home/air/office/{0!s}".format(device_key), json_out.to_json())

def on_ispindel(client, userdata, message):
    publish_client: MqttClient = userdata["internal"]
    device_key = message.topic.rpartition("/")[0].rpartition("/")[2]
    json_out = TimestampedJSONObject(dict(), "beer")
    json_out.set_timestamp(time.time())
    if "ispindel_1_mpu6050_angle_x" == device_key:
        angle_x = float(message.payload.decode())
        sg = 0.001535385 * angle_x * angle_x - 1.6805725122 * angle_x + 1109.1767657028
        json_out.data["key"] = "sg"
        json_out.data["name"] = "specific gravity"
        json_out.data["value"] = sg
        publish_client.publish_retained("home/beer/ferm/{0!s}".format("sg"), json_out.to_json())
    if "ispindel_1_battery_voltage" == device_key:
        bV = float(message.payload.decode())
        bV = bV * 4.2 / 3.3
        json_out.data["key"] = "battery-voltage"
        json_out.data["name"] = "Battery Voltage"
        json_out.data["value"] = bV
        publish_client.publish_retained("home/beer/ferm/{0!s}".format("battery-voltage"), json_out.to_json())
    json_out.data["key"] = device_key
    json_out.data["name"] = device_key
    json_out.data["value"] = message.payload.decode()
    publish_client.publish_retained("home/beer/ferm/{0!s}".format(device_key), json_out.to_json())


# master MQTT
clock = time.time()
mqtt_prod = client

# client MQTT
mqtt_internal = internal
mqtt_internal.threaded_run()

mqtt_prod.threaded_run()

mqtt_prod.client.user_data_set(dict({"internal": mqtt_internal, "prod": mqtt_prod}))
mqtt_internal.client.user_data_set(dict({"internal": mqtt_internal, "prod": mqtt_prod}))

mqtt_internal.subscribe("home/clock")
mqtt_internal.message_callback_add("home/clock", on_clock)

# The smart bulbs
mqtt_prod.subscribe("zigbee2mqtt/+")
mqtt_prod.message_callback_add("zigbee2mqtt/+", on_zigbee)

mqtt_prod.subscribe("airgradient1/sensor/#")
mqtt_prod.message_callback_add("airgradient1/sensor/#", on_airgradient)

mqtt_prod.subscribe("ispindel/sensor/#")
mqtt_prod.message_callback_add("ispindel/sensor/#", on_ispindel)


while True:
    time.sleep(10)
