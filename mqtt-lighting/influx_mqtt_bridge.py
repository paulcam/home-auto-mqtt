import logging
import time
import math
from datetime import datetime

from influxdb import InfluxDBClient

from home_auto_lib.master_clock import MasterClock
from home_auto_lib.mqtt import client
from home_auto_lib.value_objects import ExpirableData, TimestampedJSONObject

bulb_topic = "home/lighting/bulbs/+"


def write_json(json_body):
    logging.info("Write to influx: {0!s}".format(json_body))
    try:
        influx.write_points(json_body)  # , retention_policy="one_year")
    except Exception as e:
        logging.error("Error {0!s}".format(e))


def publish(userdata, message, measurement):
    topic = message.topic
    payload = message.payload
    zone = str(topic).rpartition("/")[2]
    obj = ExpirableData.from_json(payload, zone)
    data = obj.data
    timestamp = datetime.utcfromtimestamp(data["timestamp"])
    if math.isnan(float(data["value"])):
        logging.debug("NaN value received: {0!s}".format(data))
        return
    if measurement == "temperature" and (float(data["value"]) < -20 or float(data["value"]) > 84):
        logging.debug("OutOfWhack value received: {0!s}".format(data))
        return
    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": data["key"],
                "name": data["name"]
            },
            "time": timestamp,
            "fields": {
                "value": float(data["value"])
            }
        }
    ]
    write_json(json_body)


def on_bulb(mqtt_client, userdata, message):
    try:
        obj = TimestampedJSONObject.from_json(message.payload, "")
        data = obj.data
        timestamp = datetime.utcfromtimestamp(data["timestamp"])
        json_body = [
            {
                "measurement": "bulbs",
                "tags": {
                    "key": data["key"],
                    "name": data["name"],
                    "state": data["state"]
                },
                "time": timestamp,
                "fields": {
                    "state": data["state"],
                    "brightness": data["brightness"]
                }
            }
        ]
        write_json(json_body)
    except Exception as e:
        logging.error("Bulb update Failed: {0!s} - {1!r}".format(message.payload, e))
        pass


def on_occupancy(mqtt_client, userdata, message):
    try:
        obj = TimestampedJSONObject.from_json(message.payload, "")
        data = obj.data
        timestamp = datetime.utcfromtimestamp(data["timestamp"])
        json_body = [
            {
                "measurement": "occupancy",
                "tags": {
                    "key": data["key"],
                    "name": data["name"],
                },
                "time": timestamp,
                "fields": {
                    "illuminance": data["illuminance"]
                }
            }
        ]
        write_json(json_body)
    except Exception as e:
        logging.error("Lux update Failed: {0!s} - {1!r}".format(message.payload, e))
        pass

def on_lux(mqtt_client, userdata, message):
    try:
        obj = TimestampedJSONObject.from_json(message.payload, "")
        data = obj.data
        timestamp = datetime.utcfromtimestamp(data["timestamp"])
        json_body = [
            {
                "measurement": "lux",
                "tags": {
                    "key": data["key"],
                    "name": data["name"],
                },
                "time": timestamp,
                "fields": {
                    "illuminance": data["illuminance"]
                }
            }
        ]
        write_json(json_body)
    except Exception as e:
        logging.error("Lux update Failed: {0!s} - {1!r}".format(message.payload, e))
        pass

def on_air(mqtt_client, userdata, message):
    try:
        obj = TimestampedJSONObject.from_json(message.payload, "office")
        data = obj.data
        timestamp = datetime.utcfromtimestamp(data["timestamp"])
        json_body = [
            {
                "measurement": "air",
                "tags": {
                    "key": data["key"],
                    "name": data["name"],
                },
                "time": timestamp,
                "fields": {
                    "value": float(data["value"])
                }
            }
        ]
        write_json(json_body)
    except Exception as e:
        logging.error("Air update Failed: {0!s} - {1!r}".format(message.payload, e))
        pass

def on_beer(mqtt_client, userdata, message):
    try:
        obj = TimestampedJSONObject.from_json(message.payload, "beer")
        data = obj.data
        timestamp = datetime.utcfromtimestamp(data["timestamp"])

        json_body = [
            {
                "measurement": "beer",
                "tags": {
                    "key": data["key"],
                    "name": data["name"],
                },
                "time": timestamp,
                "fields": {
                    "value": float(data["value"])
                }
            }
        ]
        write_json(json_body)
    except Exception as e:
        logging.error("Beer update Failed: {0!s} - {1!r}".format(message.payload, e))
        pass


#{"timestamp": 1714948275.1191497, "hr_time": "2024-05-05 23:31:15", "key": "ispindel_1_mpu6050_angle_x", "name": "ispindel_1_mpu6050_angle_x", "value": "53.3"}
def on_battery(mqtt_client, userdata, message):
    try:
        obj = TimestampedJSONObject.from_json(message.payload, "")
        data = obj.data
        timestamp = datetime.utcfromtimestamp(data["timestamp"])
        json_body = [
            {
                "measurement": "zigbeebatteries",
                "tags": {
                    "key": data["key"],
                    "name": data["name"],
                },
                "time": timestamp,
                "fields": {
                    "battery": float(data["battery"])
                }
            }
        ]
        write_json(json_body)
    except Exception as e:
        logging.error("Lux update Failed: {0!s} - {1!r}".format(message.payload, e))
        pass


influx = InfluxDBClient("influxdb", 8087, 'root', 'root', 'home_auto', pool_size=100)

master_clock = MasterClock(client, "home/clock")
master_clock.subscribe_to_clock()
time.sleep(1)

client.subscribe(bulb_topic)
client.message_callback_add(bulb_topic, on_bulb)

client.subscribe("home/lighting/lux/+")
client.message_callback_add("home/lighting/lux/+", on_lux)

client.subscribe("home/zigbee-batteries/+")
client.message_callback_add("home/zigbee-batteries/+", on_battery)

client.subscribe("home/air/office/+")
client.message_callback_add("home/air/office/+", on_air)

client.subscribe("home/beer/ferm/+")
client.message_callback_add("home/beer/ferm/+", on_beer)

client.blocking_run()
