from home_auto_lib.mqtt_lib import MqttMultiTopicObservableCache
import logging
from home_auto_lib.mqtt import client, cache


class FermentChamberController:
    def __init__(self, mqtt_client, mqtt_cache: MqttMultiTopicObservableCache):
        self.client = mqtt_client
        self.cache = mqtt_cache
        self.chamber_temp_topic = "home/sensors/temperature/FermChamber"
        self.ferment_temp_topic = "home/beer/ferm/temperature"
        self.ferment_target_topic = "home/beer/ferm/target-temperature"
        self.fridge_cmd_topic = "cmnd/tasmota_162AD6/POWER2"
        self.cache.add_subscription_with_observer(self.chamber_temp_topic, self)
        self.cache.add_subscription_with_observer(self.ferment_temp_topic, self)
        self.cache.add_subscription_with_observer(self.ferment_target_topic, self)

    def notify(self, topic):
        chamber_temp_topic = self.cache.last_for_topic(self.chamber_temp_topic)
        ferment_temp_topic = self.cache.last_for_topic(self.ferment_temp_topic)
        ferment_target_topic = self.cache.last_for_topic(self.ferment_target_topic)
        if not chamber_temp_topic or not ferment_temp_topic or not ferment_target_topic:
            return
        required_cooling_state = False
        try:
            ch_temp = float(chamber_temp_topic["value"])
            if ch_temp < 0:
                logging.warning("Disabled cooling as chamber temp is below freezing")
                self.client.publish(self.fridge_cmd_topic, "OFF")
                return

            fm_temp = float(ferment_temp_topic["value"])
            fm_target = float(ferment_target_topic["value"])

            if fm_temp > fm_target:
                required_cooling_state = True
            logging.info("Determined fridge state to be: {0!s} CHTemp: {1!s} FMTemp: {2!s} Target:{3!s}"
                         .format("ON" if required_cooling_state else "OFF", ch_temp, fm_temp, fm_target))
            self.client.publish(self.fridge_cmd_topic, "ON" if required_cooling_state else "OFF")

        except Exception as e:
            logging.error("Failed checking ferment chamber temp. {0!s}".format(e), e)
            return


cntlr = FermentChamberController(client, cache)

client.blocking_run()
