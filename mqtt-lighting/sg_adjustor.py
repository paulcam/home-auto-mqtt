from home_auto_lib.mqtt_lib import MqttClient,  MqttMultiTopicObservableCache
import logging
from home_auto_lib.mqtt import client, cache, get_clock
from home_auto_lib.value_objects import TimestampedJSONObject
class SGAdjustor:

    def __init__(self, mqtt_client: MqttClient, mqtt_cache:  MqttMultiTopicObservableCache):
        self.client = mqtt_client
        self.cache = mqtt_cache
        self.temp_topic = "home/beer/ferm/temperature"
        self.x_topic = "home/beer/ferm/ispindel_1_mpu6050_angle_x"
        self.sg_adj_topic = "home/beer/ferm/sg_calibrated"
        self.high_calibration_sg_topic = "home/beer/ferm/high_calibration"
        self.low_calibration_sg_topic = "home/beer/ferm/low_calibration"
        self.high_calibration = {"x_angle": 44.1, "sg_value": 1047}
        self.low_calibration = {"x_angle": 66.3, "sg_value": 1000}
        self.calibration_slope = self.calculate_slope(44.1, 1047, 67, 1000 )
        self.calibration_y_intercept = self.calculate_y_intercept(44.1, 1047,self.calibration_slope )

        mqtt_cache.add_subscription_with_observer(self.high_calibration_sg_topic, self)
        mqtt_cache.add_subscription_with_observer(self.low_calibration_sg_topic, self)
        mqtt_cache.add_subscription_with_observer(self.x_topic, self)

    @staticmethod
    def calculate_slope(x1, y1, x2, y2):
        return (y2 - y1) / (x2 - x1)

    @staticmethod
    def calculate_y_intercept(x1, y1, slope):
        return y1 - slope * x1

    @staticmethod
    def extrapolate_y(x, slope, y_intercept):
        return slope * x + y_intercept

    @staticmethod
    def extrapolate_x(y, slope, y_intercept):
        return (y - y_intercept) / slope

    def process_angle_update(self, topic):
        x_angle_obj = self.cache.last_for_topic(topic)
        if not x_angle_obj:
            logging.warning("Failed to receive valid angle_x object")
            return
        if "value" not in x_angle_obj:
            logging.warning("Failed to receive valid angle_x value")
            return
        x_angle = float(x_angle_obj["value"])

        sg = self.extrapolate_y(x_angle, self.calibration_slope, self.calibration_y_intercept)
        logging.info("SG Calculated as {0!s}".format(sg))
        msg = TimestampedJSONObject({"key": "sg_calibrated",
                                          "name": "sg_calibrated",
                                          "value": sg,
                                          "zone": "beer"}, "beer")
        msg.set_timestamp(x_angle_obj["timestamp"])
        self.client.publish(self.sg_adj_topic, msg.to_json())


    def process_calibration(self, topic):
        calibration_obj = self.cache.last_for_topic(topic)
        if not calibration_obj:
            logging.warning("Failed to receive valid calibration object")
            return
        if "x_angle" not in calibration_obj:
            logging.warning("Failed to receive valid x_angle ")
            return
        calibration_x_angle = float(calibration_obj["x_angle"])
        if "sg_value" not in calibration_obj:
            logging.warning("Failed to receive valid sg_value ")
            return
        calibration_sg_value = float(calibration_obj["sg_value"])

        if not calibration_sg_value or not calibration_x_angle:
            logging.warning("Failed to find both sg_value and x_angle to calibrate with")
            return

        if "high" in topic:
            self.high_calibration = calibration_obj
        elif "low" in topic:
            self.low_calibration = calibration_obj
        else:
            logging.warning("Unexpected calibration topic, no high/low")
            return
        if not self.high_calibration or not self.low_calibration:
            logging.warning("Not enough calibration points.")
            return

        self.calibration_slope = self.calculate_slope(self.low_calibration["x_angle"],
                                                      self.low_calibration["sg_value"],
                                                      self.high_calibration["x_angle"],
                                                      self.high_calibration["sg_value"])
        logging.info("Calibration Slope Calculated as {0!s} from x1 {1!s}, sg1 {2!s}, x2 {3!s}, sg2 {4!s}"
                     .format(self.calibration_slope,
                     self.low_calibration["x_angle"],
                     self.low_calibration["sg_value"],
                     self.high_calibration["x_angle"],
                     self.high_calibration["sg_value"]))

        self.calibration_y_intercept = self.calculate_y_intercept(self.low_calibration["x_angle"],
                                                      self.low_calibration["sg_value"],
                                                      self.calibration_slope)

        logging.info("Calibration Y Intercept Calculated as {0!s} from x1 {1!s}, sg1 {2!s}, slope {3!s}"
                     .format(self.calibration_y_intercept,
                             self.low_calibration["x_angle"],
                             self.low_calibration["sg_value"],
                             self.calibration_slope))
        self.process_angle_update(self.x_topic)

    def notify(self, topic):
        logging.debug("Notified about topic:{0!s}".format(topic))

        if self.x_topic in topic:
            self.process_angle_update(topic)
        elif self.high_calibration_sg_topic in topic or self.low_calibration_sg_topic:
            self.process_calibration(topic)


ctr = SGAdjustor(client, cache)

client.blocking_run()
