import json
from home_auto_lib.master_clock import MasterClock

from home_auto_lib.mqtt_lib import MqttClient, HAAwareMqttClient, MqttMultiTopicObservableCache
import logging
from home_auto_lib.mqtt import client, cache, get_clock


class ZigbeeSwitchControlListener:
    def __init__(self, mqtt_client: MqttClient, mqtt_cache:  MqttMultiTopicObservableCache):
        self.client = mqtt_client
        self.cache = mqtt_cache

    def notify(self, topic):
        logging.debug("Notified about topic:{0!s}".format(topic))

        action = self.cache.last_for_topic(topic)
        logging.debug("Payload for lot:{0!s} = {1!s}".format(topic, action))

        topic_path = topic.split("/")
        device = topic_path[-2]
        if "_switch" in device:
            logging.debug("{0!s} looks like a switch".format(device))
        else:
            logging.debug("{0!s} not a switch ignoring".format(device))
            return
        switch_zone = device.replace("_switch","")
        logging.debug("Declared Switch Zone: {0!s}".format(switch_zone))

        control_zones = self.determine_control_zone(switch_zone, action)

        for control_zone in control_zones:
            control_topic = "home/lights/control-states/{0!s}".format(control_zone)
            # Ensure we are subscribed to the control topic
            self.cache.add_subscription(control_topic)
            # Check cache for latest state
            zone_state = self.cache.last_for_topic(control_topic)
            try:
                zone_state
            except NameError:
                zone_state = self.default_control_state(control_zone)

            if not zone_state or zone_state is None:
                zone_state = self.default_control_state(control_zone)

            current = zone_state["state"]
            zone_state["state"] = zone_state["next-state"]
            if current == "OFF":
                zone_state["next-state"] = "ON"
            if current == "AUTO":
                zone_state["next-state"] = "AUTO"
            if current == "ON":
                zone_state["next-state"] = "OFF"
            self.client.publish_retained(control_topic, json.dumps(zone_state))

        # TODO look for existing matching control state topic
        #  IF found toggle through the sequence ON, AUTO, OFF, AUTO and publish
        #  IF not found create a control state set to AUTO and publish
        #  ^^^ this is where 'profiles' come in to select the default state?
        #  Maybe later.

        # Until we have to manage conflicting state requirements that is about it

    @staticmethod
    def default_control_state(control_zone):
        state = {
            "zone-name": control_zone,
            "state": "OFF",
            "next-state": "AUTO",
            "timestamp": get_clock()
        }
        return state

    @staticmethod
    def determine_control_zone(switch_zone, action):
        if switch_zone == "test":
            if action == "single":
                return ["office-overhead"]
            if action == "double":
                return ["office-wled"]
        return []


ctr = ZigbeeSwitchControlListener(client, cache)

cache.add_subscription_with_observer("zigbee2mqtt/+/action", ctr)

client.blocking_run()
