from home_auto_lib.mqtt_lib import MqttClient, MqttMultiTopicObservableCache
import time
import datetime
import json
import logging
from home_auto_lib.mqtt import client, cache, get_clock

from home_auto_lib.logging_setup import setup_logging

solar_panel_power_topic = "solar/panel"
alt_solar_panel_topic = "venus-home/N/b827eb3e8f99/solarcharger/289/Yield/Power"


class KitchenLightController:
    def __init__(self, mqtt_client, mqtt_cache:MqttMultiTopicObservableCache, config_topic, solar_power_topic):
        global alt_solar_panel_topic
        self.config_topic = config_topic
        self.solar_power_topic = solar_power_topic
        self.client = mqtt_client
        self.cache = mqtt_cache
        self.kitchen_motion_topic = "zigbee2mqtt/kitchen_motion"
        self.cache.add_subscription(self.kitchen_motion_topic)
        self.cache.add_observer(self.kitchen_motion_topic, self)
        self.cache.add_subscription(self.solar_power_topic)
        self.cache.add_subscription( alt_solar_panel_topic )

        # default config
        self.config = {"panel_power": 4, "day_brightness": 255, "night_brightness": 127}
        self.cache.add_subscription_with_observer(self.config_topic, self)

        # No observer for panel voltage.  Does not trigger events.
    def get_panel_power(self):
        global alt_solar_panel_topic
        panel_1_power = self.cache.last_for_topic(self.solar_power_topic)
        panel_2_power = self.cache.last_for_topic(alt_solar_panel_topic)
        panel1_value = 0
        panel2_value = 0
        if panel_1_power:
            if "P" in panel_1_power:
                panel1value = float(panel_1_power["P"])
        if panel_2_power:
            panel2_value = float(panel_2_power["value"])
        logging.debug("Panel1Value: "+str(panel1_value)+", Panel2Value: "+str(panel2_value))
        return max( panel1_value, panel2_value )

    def notify(self, topic):
        payload = self.cache.last_for_topic(topic)

        if topic == self.config_topic:
            logging.debug("Config Update %s", payload)
            self.config = payload
            return

        if topic == self.kitchen_motion_topic:
            logging.debug("Kitchen motion message, payload: {0!s}".format(payload))
            bulbs = ["kitchen_ctr_bulb","kitchen_rr_bulb","kitchen_rl_bulb","kitchen_fl_bulb","kitchen_fr_bulb"]
        else:
            return

        if "occupancy" in payload:
            occupancy = payload["occupancy"]
            if occupancy:
                panel_power = self.get_panel_power()
                if panel_power > self.config["panel_power"]:
                    logging.debug("Still Daylight.")
                    return

                self.turnon(bulbs)
            else:
                self.turnoff(bulbs)

    def turnon(self, bulbs):
        for bulb in bulbs:
            if "lamp" in bulb:
                topic = "{0!s}/switch/sonoff_basic_relay/command".format(bulb)
                payload = "ON"
            else:
                topic = "zigbee2mqtt/{0!s}/set".format(bulb)
                brightness = self.config["day_brightness"]
                transition = 0.5
                now = datetime.datetime.now()
                hour = now.hour
                if hour < 7:
                    brightness = self.config["night_brightness"]
                    transition = 10

                payload = json.dumps({
                    "state": "ON",
                    "brightness": brightness,
                    "transition": transition
                })
            self.client.publish(topic, payload)

    def turnoff(self, bulbs):
        for bulb in bulbs:
            if "lamp" in bulb:
                topic = "{0!s}/switch/sonoff_basic_relay/command".format(bulb)
                payload = "OFF"
            else:
                topic = "zigbee2mqtt/{0!s}/set".format(bulb)
                payload = json.dumps({
                    "state": "OFF",
                    "transition": 10
                })
            self.client.publish(topic, payload)


hall_cntlr = KitchenLightController(client, cache, "home/config/lights/kitchen", solar_panel_power_topic)

client.blocking_run()
