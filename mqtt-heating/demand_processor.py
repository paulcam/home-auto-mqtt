from home_auto_lib.mqtt_lib import MqttClient, MqttMultiTopicObservableCache
from home_auto_lib.value_objects import Demand, Control
import time
import json
import logging
from home_auto_lib.logging_setup import setup_logging
setup_logging()
import home_auto_lib.master_clock

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)

class HeatingDemandProcessor:
    def __init__(self, client,
                 demand_topic_root="home/heating/demands"):
        self.logger = module_logger.getChild(self.__class__.__name__)
        self.logger.addHandler(logging.NullHandler())

        self.client = client
        self.demand_topic_root = demand_topic_root
        demand_topic = demand_topic_root+"/+"
        on_change = "timestamp"
        self.logger.debug("Subscribing to {0!s}".format(demand_topic))
        self.cache = MqttMultiTopicObservableCache(self.client, [ demand_topic ], on_change)
        self.handlers = dict()

    def add_handler(self, handler):
        self.logger.info("Adding new handler. {0!s}".format(handler))
        topic_expression = handler.demand_expression
        handler.processor = self
        self.handlers[topic_expression] = handler
        self.cache.add_subscription(topic_expression)
        self.cache.add_observer(topic_expression, handler)


class DemandHandler:
    def __init__(self, demand_expression):
        self.processor=None
        self.demand_expression = demand_expression
        self.logger = module_logger.getChild(self.__class__.__name__)
        self.logger.addHandler(logging.NullHandler())

    def __str__(self):
        return "DemandHandler for {0!s}".format(self.demand_expression)

    def notify(self, topic):
        global clock
        if not self.processor:
            self.logger.warn("Notify called but no processor available yet.")
            return
        zone = str(topic).rpartition( "/")[2]
        if not zone:
            self.logger.warn("Notify called with null zone.")
            return
        demand_topic = self.processor.demand_topic_root + "/" + zone
        demand = self.processor.cache.last_for_topic(demand_topic)
        if not demand:
            self.logger.debug("No existing demand on topic {0!s}".format(demand_topic))
            return
        demand = Demand(demand, zone)
        self.logger.debug("Existing demand on topic {0!s} {1!s}".format(demand_topic, demand))
        if demand.is_expired(clock):
            return
        self.handle(demand, zone)

    def handle(self, demand, zone):
        self.logger.warn("Handle called on abstract handler.")
        pass

    def accept_demand(self, demand, current_control):
        self.logger.warn("accept_demand called on abstract handler.")
        pass

    def handle_demand(self, demand, zone, control_topic, control_state, expiry):
        global clock
        new_control = {
            "state": control_state,
            "expiry": expiry,
            "metadata": {"demand": demand.data},
            "timestamp": clock,
            "hr_time": time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(clock))

        }

        current_control = self.processor.cache.last_for_topic(control_topic)
        if current_control:
            current_control = Control(current_control, zone)
            self.logger.debug("Current control demand found: {0!s}".format(current_control))
        else:
            self.logger.debug("No current control.")
            current_control = None

        self.logger.debug("Control target state: {0!s}".format(new_control["state"]))

        if self.accept_demand(demand, current_control):
            self.logger.debug("Demand accepted: {0!s}".format(demand))
            self.logger.info("Publishing control: {0!s}".format(new_control))
            self.processor.cache.client.publish_retained(control_topic, json.dumps(new_control))


class RadiatorDemandHandler(DemandHandler):
    def __init__(self, control_topic, demand_expression):
        super().__init__(demand_expression)
        self.control_topic=control_topic

    def __str__(self):
        return "Control Topic {0!s} demand expression {1!s}".format(self.control_topic, self.demand_expression)

    def handle(self, demand, zone):
        global clock
        self.logger.debug("Radiator handling demand for {0!s}".format(zone))
        self.handle_demand(demand, zone, self.control_topic,
                           "OPEN" if demand.data["state"] == "ON" else "CLOSED",
                           120)


    def accept_demand(self, demand, current_control):
        # No conflicting control
        if not current_control:
            self.logger.debug("No current control. accepted!")
            return True

        demand_state = "OPEN" if demand.data["state"] == "ON" else "CLOSED",
        self.logger.debug("Target radiator state: {0!s}".format(demand_state))

        if not current_control.is_expired(clock):
            if current_control.data["state"] != demand_state:
                if demand_state == "OPEN":
                    self.logger.debug("Not allowed to override state of a valid CLOSED control {0!s} with demand {1!s}".format(current_control, demand))
                    return False
                else: # CLOSED so demand accepted.
                    self.logger.info("Allowing state change to CLOSED {0!s} with demand {1!s}".format(
                        current_control, demand))
                    return True

            self.logger.info("Refreshing existing control for same state {0!s} with demand {1!s}".format(current_control, demand))
            return True
        else: # is expired
            self.logger.info("Current control expired, accepting demand.")
            return True


class BoilerDemandHandler(DemandHandler):
    def __init__(self, control_topic, demand_expression):
        super().__init__(demand_expression)
        self.control_topic=control_topic

    def __str__(self):
        return "Control Topic {0!s} demand expression {1!s}".format(self.control_topic, self.demand_expression)

    def handle(self, demand, zone):
        global clock
        self.logger.debug("Handling demand for {0!s}".format(zone))
        self.handle_demand(demand, zone, self.control_topic,
                           "ON" if demand.data["state"] == "ON" else "OFF",
                           600)

    def accept_demand(self, demand, current_control):
        # No conflicting control
        if not current_control:
            return True

        demand_state = demand.data["state"]
        if not current_control.is_expired(clock):
            if current_control.data["state"] != demand_state:
                if demand_state == "ON":
                    self.logger.debug("Not allowed to override state of a valid OFF control {0!s} with demand {1!s}".format(current_control, demand))
                    return False
                else: # OFF so demand accepted.
                    self.logger.info("Allowing state change to OFF {0!s} with demand {1!s}".format(
                        current_control, demand))
                    return True

            self.logger.info("Refreshing existing control for same state {0!s} with demand {1!s}".format(current_control, demand))
            return True
        else: # is expired
            self.logger.info("Current control expired, accepting demand.")
            return True



def on_clock(client, userdata, message):
    global clock
    clock_obj = json.loads(message.payload.decode("UTF-8"))
    if "utctime" in clock_obj:
        utctime = clock_obj["utctime"]
        clock = float(utctime)

mqtt = MqttClient(client_name="DemandProcessor", broker_port=11883)
cache = MqttMultiTopicObservableCache(mqtt, [], "value")
time.sleep(5)
clock = time.time()
mqtt.subscribe("home/clock")
mqtt.message_callback_add("home/clock", on_clock)

boilerHandler = BoilerDemandHandler("home/heating/control/boiler/main", "home/heating/demands/+")

processor = HeatingDemandProcessor(mqtt)
processor.add_handler(boilerHandler)

officeRadHandler = RadiatorDemandHandler("home/heating/control/radiator/office", "home/heating/demands/office")
processor.add_handler(officeRadHandler)
lvrRadHandler = RadiatorDemandHandler("home/heating/control/radiator/livingRoom", "home/heating/demands/livingRoom")
processor.add_handler(lvrRadHandler)
bdrRadHandler = RadiatorDemandHandler("home/heating/control/radiator/bedroom", "home/heating/demands/bedroom")
processor.add_handler(bdrRadHandler)
time.sleep(2)
mqtt.blocking_run()
