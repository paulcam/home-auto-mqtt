from home_auto_lib.mqtt_lib import MqttClient, MqttTopicQueue, MqttTopicCache
from home_auto_lib.value_objects import Request, Demand
from threading import Thread
import time
import json
import logging
from home_auto_lib.logging_setup import setup_logging
setup_logging()

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)

def on_clock(client, userdata, message):
    global clock
    clock_obj = json.loads(message.payload.decode("UTF-8"))
    if "utctime" in clock_obj:
        utctime = clock_obj["utctime"]
        clock = float(utctime)


mqtt = MqttClient(client_name="RequestProcessor", broker_port=11883)
mqtt.threaded_run()
clock = time.time()
mqtt.subscribe("home/clock")
mqtt.message_callback_add("home/clock", on_clock)


class RequestProcessor(Thread):
    def __init__(self, client, rules, request_topic, demand_topic):
        super().__init__()
        self.client = client
        self.rules = rules
        self.request_topic = request_topic
        self.demand_topic = demand_topic
        self.requests = MqttTopicQueue(client, request_topic)
        self.demands = MqttTopicCache(client, demand_topic)

    def run(self):
        while True:
            try:
                module_logger.debug("Waiting on request message...")
                topic,payload = self.requests.pop()
                print("Got request message {0!s} {1!s}".format(topic, payload))
                if len(topic) > 0 and len(payload)>0:
                    self.process_message(topic, payload)
            except KeyboardInterrupt:
                return
            except Exception as e:
                print("Exception {0!s} - {1!s}".format(e, e.__traceback__))

    def process_message(self, topic, payload):
        zone = str(topic).rpartition("/")[2]
        print("Request Topic {0!s} Zone {1!s}".format(topic, zone))
        demand_base_topic = str(self.demand_topic).rpartition("/")[0]
        demand_topic = demand_base_topic+"/"+zone
        print("Demand Topic {0!s}".format(demand_topic))
        demand_str = self.demands.get_last_for_topic(demand_topic)

        # Unmarshall JSON
        if demand_str:
            existing_demand = Demand.from_json(demand_str, zone)
        else:
            existing_demand = None
        request = Request.from_json(payload, zone)

        accept, demand = self.rules.accept_request(topic, request, existing_demand)
        if accept:
            self.client.publish_retained(demand_topic, demand.to_json())


class RequestDemandRules:
    def __init(self):
        pass

    def accept_request(self, topic, request, demand):
        global clock
        if not request: return False, None

        if request.is_expired(clock):
            print("Request has expired {0!s}".format(request))
            return False, None

        if not demand:
            print("No matching demand for request: {0!s} accepting request - {1!s}".format(topic, demand))
            return True, Demand.from_request(request)

        if demand.is_expired(clock):
            print("Previous Demand has expired {0!s}".format(demand))
            return True, Demand.from_request(request)

        if demand.data["state"] != request.data["state"]:
            print("Forbidden to change state of previous Demand {0!s} with {1!s}".format(demand, request))
            return False, None

        demand_target = demand.target()
        request_target = request.target()
        if demand_target and request_target:
            if float(demand_target) > float(request_target):
                print("Not overriding Demand with target temp {0!s} with request for temp {1!s}"
                      .format(demand_target, request_target))
                return False, None
            elif float(demand_target) < float(request_target):
                print("Overriding Demand with target temp {0!s} with request for temp {1!s}"
                      .format(demand_target, request_target))
                return True, Demand.from_request(request)
            else: # same temp
                if demand.when_expires(clock) < request.when_expires(clock):
                    print("Refreshing Demand with same target temp {0!s} as request for temp {1!s}"
                      .format(demand_target,request_target))
                return True, Demand.from_request(request)

        if request_target:
            print("Not overriding Demand with NO target temp {0!s} with request for temp {1!s}"
                  .format(demand_target, request_target))
            return False, None
        if demand_target:
            print("Overriding Demand with target temp {0!s} with request for no target temp {1!s}"
                  .format(demand_target, request_target))
            return True, Demand.from_request(request)

        if not demand_target and not request_target:
            if demand.when_expires(clock) < request.when_expires(clock):
                print("Refreshing Demand with no target temp {0!s} as request with no temp {1!s}"
                      .format(demand_target, request_target))
            return True, Demand.from_request(request)

        print("Found no reason to accept request. {0!s}".format(request))
        return False, None






rules = RequestDemandRules()
req = RequestProcessor(mqtt, rules, "home/heating/requests/+", "home/heating/demands/+")
print("Giving the demand cache time to populate...")
time.sleep(3) # sleep to allow the cache time to fill with retained demands.
print("Starting work...")
req.start()
