from home_auto_lib.mqtt_lib import MqttClient
from home_auto_lib.value_objects import TimestampedJSONObject
import time
import json
from home_auto_lib.mqtt import client, internal


def on_clock(client, userdata, message):
    global clock
    clock_obj = json.loads(message.payload.decode("UTF-8"))
    if "utctime" in clock_obj:
        utctime = clock_obj["utctime"]
        clock = float(utctime)


def republish_sonoff_temp_humidity(client, userdata, message, topic_base):
    global clock
    if len(message.payload) < 1:
        return
    incoming_topic = message.topic
    zone = message.topic


def republish_client(client, userdata, message):
    pass
    client.publish(message.topic, message.payload)


def sonoff_sensor_publish(mqtt, userdata, message, zone, name):
    global clock
    publish_client: MqttClient = userdata["internal"]
    try:
        json_obj = TimestampedJSONObject(json.loads(message.payload.decode("UTF-8")), zone)
    except:
        pass
    # FIXME!
    json_obj.data["key"] = zone
    json_obj.data["name"] = name

    json_obj.set_timestamp(clock)
    json_obj.data["value"] = json_obj.data["temperature"]

    payload = json.dumps(json_obj.data)
    #print(payload)
    publish_client.publish_retained("home/sensors/temperature/"+zone, payload)

    json_obj.data["value"] = json_obj.data["humidity"]

    payload = json.dumps(json_obj.data)
   # print(payload)
    publish_client.publish_retained("home/sensors/humidity/"+zone, payload)


def sonoff_sensor_detect(mqtt, userdata, message):
    topic = str(message.topic).rpartition("/")[2]
    if str(topic).endswith("_th"):
        zone = str(topic).rpartition("_")[0]
        sonoff_sensor_publish(mqtt, userdata, message, zone, zone.capitalize())


# master MQTT
mqtt_prod = client

# client MQTT
mqtt_internal = internal


mqtt_internal.subscribe("home/clock")
mqtt_internal.message_callback_add("home/clock", on_clock)

# Zigbee-SonOff-TH sensors
mqtt_prod.subscribe("zigbee2mqtt/+")
mqtt_prod.message_callback_add("zigbee2mqtt/+", sonoff_sensor_detect)

mqtt_prod.threaded_run()
mqtt_internal.threaded_run()

mqtt_prod.client.user_data_set(dict({"internal": mqtt_internal, "prod": mqtt_prod}))
mqtt_internal.client.user_data_set(dict({"internal": mqtt_internal, "prod": mqtt_prod}))

while True:
    time.sleep(10)
