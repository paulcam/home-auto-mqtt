import math
import json
import logging
import time
import re
from datetime import datetime

from influxdb import InfluxDBClient

from home_auto_lib.master_clock import MasterClock
from home_auto_lib.mqtt_lib import MqttClient
from home_auto_lib.value_objects import ExpirableData,TimestampedJSONObject, Demand, Control
from home_auto_lib.mqtt import client, get_clock
from queue import Queue
import queue

logging.basicConfig(level=logging.DEBUG,
                    format="%(asctime)s %(levelname)s %(message)s [%(filename)s:%(lineno)d]")
influx_client = InfluxDBClient("influxdb", 8087, 'root', 'root', 'home_auto', pool_size=100)


temp_topic = "home/sensors/temperature/+"
rh_topic = "home/sensors/humidity/+"
sch_topic = "home/heating/schedules/+"
tts_topic = "home/heating/active_targets/+"
control_topic = "home/heating/control/+/+"
demand_topic = "home/heating/demands/+"
presence_topic = "home/presence/+"

influx_queue = Queue(maxsize=1000)


class TopicBridge:
    def __init__(self, measurement_name):
        self.measurement_name = measurement_name

    # This default variant processes correctly formed TimestampedJSON objects with a value etc.
    def on_message(self, mqtt_client, userdata, message):
        try:
            topic = message.topic
            payload = message.payload
            zone = str(topic).rpartition("/")[2]
            obj = TimestampedJSONObject.from_json(payload, zone)
            if not obj.validate(get_clock()):
                return

            if not obj.is_numeric():
                logging.error("NaN value received: {0!s}".format(obj.data))
                return

            self.publish(obj)
        except Exception as e:
            logging.error("Failed to handle message on topic <{0!s}> with payload <{1!s}> got exception".format(topic, payload), None, e)
            return

    def publish(self, timestamped_obj:TimestampedJSONObject):
        if not self.validate_outbound(timestamped_obj):
            logging.error("Outbound message failed validation <{0!s}>".format(timestamped_obj))
            return
        logging.debug("Successful validation publishing <{0!s}>".format(timestamped_obj))
        timestamp = datetime.utcfromtimestamp(timestamped_obj.get_timestamp())
        json_body = [
            {
                "measurement": self.measurement_name,
                "tags": {
                    "key": timestamped_obj.get_field("key"),
                    "name": timestamped_obj.get_field("name")
                },
                "time": timestamp,
                "fields": {
                    "value": float(timestamped_obj.get_field("value"))
                }
            }
        ]
        write_json(json_body)

    def validate_outbound(self, timestamped_obj):
        return True


class TemperatureTopicBridge(TopicBridge):
    def __init__(self):
        super().__init__("temperature")

    def validate_outbound(self, timestamped_obj):
        if float(timestamped_obj.get_field("value")) < -20 or float(timestamped_obj.get_field("value")) > 84:
            logging.error("OutOfWhack value received: {0!s}".format(timestamped_obj))
            return False
        return True


class HumidityTopicBridge(TopicBridge):
    def __init__(self):
        super().__init__("humidity")

    def validate_outbound(self, timestamped_obj):
        if float(timestamped_obj.get_field("value")) < 0 or float(timestamped_obj.get_field("value")) > 100:
            logging.error("OutOfWhack value received: {0!s}".format(timestamped_obj))
            return False
        return True

class SchedulesTopicBridge(TopicBridge):
    def __init__(self):
        super().__init__("schedules")

    def on_message(self, mqtt_client, userdata, message):
        payload = message.payload
        data = json.loads(payload)
        timestamp = datetime.utcfromtimestamp(data["timestamp"])
        json_body = [
            {
                "measurement": self.measurement_name,
                "tags": {
                    "key": str(data["name"]).replace(" ", ""),
                    "name": data["name"],
                    "targetProfile": data["targetProfile"]
                },
                "time": timestamp,
                "fields": {
                    "value": bool(data["active"])
                }
            }
        ]
        write_json(json_body)

def on_tts_message(mqtt_client, userdata, message):
    publish_tts(userdata, message, "active_targets")


def on_control_message(mqtt_client, userdata, message):
    publish_control(userdata, message, "control_states")

def on_demand_message(mqtt_client, userdata, message):
    publish_demand(userdata, message, "demand_states")

def on_presence_message(mqtt_client, userdata, message):
    publish_presence(userdata, message, "presence")


class DemandsTopicBridge(TopicBridge):
    def __init__(self):
        super().__init__("demand_states")

    def on_message(self, mqtt_client, userdata, message):
        payload = message.payload
        data = json.loads(payload)
        demand = Demand.from_json(payload)
        zone = demand.data["zone"]
        now = get_clock()
        state = str(demand.data["state"])
        dt = datetime.utcfromtimestamp(demand.data["timestamp"])

        if demand.is_expired(now):
            logging.debug("Demand State Expired {0!s} for {1!s}".format(demand.data["expiry"], zone))
            state = "OFF"
            dt = datetime.utcnow()

        json_body = [
            {
                "measurement": self.measurement_name,
                "tags": {
                    "key": zone,
                },
                "time": dt,
                "fields": {
                    "value": state,
                    "current": demand.current(),
                    "target": demand.target(),
                    "schedule": demand.schedule()
                }
            }
        ]
        write_json(json_body)


class ControlTopicBridge(TopicBridge):
    def __init__(self):
        super().__init__("control_states")

    def on_message(self, mqtt_client, userdata, message):
        topic = message.topic
        payload = message.payload
        control = Control.from_json(payload)
        now = get_clock()
        state = str(control.data["state"])
        dt = datetime.utcfromtimestamp(control.data["timestamp"])
        type = str(topic.split("/")[-2])
        device = str(topic.split("/")[-1])

        if control.is_expired(now):
            logging.debug("Control State Expired {0!s} for {1!s}".format(control.data["expiry"], type+" "+device))
            state = "OFF"
            dt = datetime.utcnow()

        json_body = [
            {
                "measurement": self.measurement_name,
                "tags": {
                    "key": device,
                    "type": type
                },
                "time": dt,
                "fields": {
                    "value": state,
                }
            }
        ]
        write_json(json_body)


def write_json(json_body):
    if influx_queue.full():
        logging.warning("Queue Full!  Size: {0!s}".format(influx_queue.qsize()))
        write_to_influx(0)

    influx_queue.put(json_body[0])


class TargetTempTopicBridge(TopicBridge):
    def __init__(self):
        super().__init__("active_targets")

    def on_message(self, mqtt_client, userdata, message):
        topic = message.topic
        payload = message.payload
        data = json.loads(payload)
        timestamp = datetime.utcfromtimestamp(data["timestamp"])
        target_zone = str(topic.rpartition("/")[2])

        json_body = [
            {
                "measurement": self.measurement_name,
                "tags": {
                    "key": target_zone,
                    "schedule": data["schedule"],
                    "sympathetic": data["sympathetic"]
                },
                "time": timestamp,
                "fields": {
                    "value": float(data["target"])
                }
            }
        ]
        write_json(json_body)


class PresenceTopicBridge(TopicBridge):
    def __init__(self):
        super().__init__("presence")

    def on_message(self, mqtt_client, userdata, message):
        topic = message.topic
        payload = message.payload
        data = json.loads(payload)
        timestamp = datetime.utcfromtimestamp(data["timestamp"])
        zone = str(topic.split("/")[-1])
        json_body = [
            {
                "measurement": self.measurement_name,
                "tags": {
                    "key": str(zone),
                },
                "time": timestamp,
                "fields": {
                    "value": bool(data["isPresent"])
                }
            }
        ]
        write_json(json_body)


def write_to_influx(clock_val):
    global influx_queue
    if influx_queue.empty():
        logging.debug("Nothing to write to influx")
        return
    json_records = []
    try:
        while record := influx_queue.get(block=False):
            json_records.append(record)
    except queue.Empty:
        logging.debug("Queue drained")

    logging.debug("Write to influx: {0!s} records".format(len(json_records)))
    try:
        # if isinstance(client, HAAwareMqttClient):
        #     if client.ha_mode == "active":
        #         influx_client.write_points(json_records)
        #     else:
        #         logging.debug("HA Client not active.  Not publishing to influx.")
        # else:
        influx_client.write_points(json_records)

    except Exception as e:
        print("Error {0!s}".format(e))


temp_ts_bridge = TemperatureTopicBridge()
client.subscribe(temp_topic)
client.message_callback_add(temp_topic, temp_ts_bridge.on_message)

rh_ts_bridge = HumidityTopicBridge()
client.subscribe(rh_topic)
client.message_callback_add(rh_topic, rh_ts_bridge.on_message)

sch_bridge = SchedulesTopicBridge()
client.subscribe(sch_topic)
client.message_callback_add(sch_topic, sch_bridge.on_message)

demand_bridge = DemandsTopicBridge()
client.subscribe(demand_topic)
client.message_callback_add(demand_topic, demand_bridge.on_message)

control_bridge = ControlTopicBridge()
client.subscribe(control_topic)
client.message_callback_add(control_topic, control_bridge.on_message)

target_temps_bridge = TargetTempTopicBridge()
client.subscribe(tts_topic)
client.message_callback_add(tts_topic, target_temps_bridge.on_message)

presence_bridge = PresenceTopicBridge()
client.subscribe(presence_topic)
client.message_callback_add(presence_topic, presence_bridge.on_message)

client.threaded_run()
while True:
    time.sleep(1)
    write_to_influx(0)
