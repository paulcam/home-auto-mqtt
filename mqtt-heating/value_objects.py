import json
import copy

class JSONObject:
    def __init__(self, data, zone):
        self.data = data
        self.zone = zone

    @classmethod
    def from_json(cls, data_str, zone):
        return cls( json.loads(data_str), zone)

    def to_json(self):
        return json.dumps(self.data)

    def __str__(self):
        return self.zone + " " + str(self.data)

    def __repr__(self):
        return "{0!s}: {1!s}".format(type(self).__name__, str(self))


class Demand(JSONObject):
    def when_expires(self, now):
        if not self.data:
            return -1
        if "timestamp" not in self.data:
            return -1
        if "expiry" not in self.data:
            return -1
        now = float(now)
        then = float(self.data["timestamp"])
        how_long = float(self.data["expiry"])
        when = then + how_long
        return when

    def is_expired(self, now):
        when = self.when_expires(now)
        if when == -1:
            return True
        if when >= now:
            return False
        else:
            return True

    def target_temp(self):
        if "metadata" in self.data:
            data = self.data["metadata"]
            if "targetTemp" in data:
                target_temp = data["targetTemp"]
                return target_temp
        return None

    def add_metadata(self, key, object):
        if not "metadata" in self.data:
            self.data["metadata"] = dict()
        self.data["metadata"][key]=object

    @classmethod
    def from_request(cls, request):
        demand_data = copy.deepcopy(request.data)
        if "metadata" in demand_data:
            metadata = demand_data["metadata"]
            metadata.pop("schedule")
            metadata["request"] = request.data
        else:
            metadata = { "request": request.data}
            demand_data["metadata"] = metadata

        return cls(demand_data, request.zone)

class Request(Demand):
    pass

class Control(Demand):
    pass