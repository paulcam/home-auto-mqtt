import json
import time

from home_auto_lib.mqtt_lib import MqttClient, MqttMultiTopicObservableCache
from home_auto_lib.master_clock import MasterClock
from threading import Timer, Lock
from home_auto_lib.logging_setup import setup_logging
import logging
import os
import subprocess

setup_logging()

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)
base_topic = "home/presence/"
domain = ".lan.campbell-multimedia.co.uk"


def ping(host):
    """
    Returns True if host (str) responds to a ping request.
    Remember that a host may not respond to a ping (ICMP) request even if the host name is valid.
    """

    params = []
    # Building the command. Ex: "ping -c 1 google.com"
    command = ['/usr/bin/ping', '-c', '1', '-W', '1', host]
    print("host:" + host)
    return subprocess.call(command, stderr=open(os.devnull, 'wb'), stdout=open(os.devnull, 'wb')) == 0


class PingPresence():

    def isHostOnline(self, host_address):
        module_logger.debug("Testing host: {0!s}".format(host_address))
        result = ping(host_address)
        module_logger.info("Host: {0!s} is {1!s}".format(host_address, result and "ONLINE" or "OFFLINE"))
        return result


def publish_presence(mqtt, isPresent, zone):
    topic = base_topic + zone
    payload = {
        "timestamp": get_clock(),
        "hr_time": time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(get_clock())),
        "isPresent": True if isPresent else False
    }

    mqtt.publish(topic, json.dumps(payload))


def get_clock():
    return MasterClock.instance.get_current_time()


some_lock = Lock()


def update_presence_detection(mqtt):
    # Am I home?
    ping_presence = PingPresence()

    with some_lock:
        # Phone
        # 5E:43:48:75:4D:D3
        # presence = snmp_presence.isMacOnline('0x8058f81a483b')
        presence = ping_presence.isHostOnline("mobile")
        publish_presence(mqtt, presence, "home")

        presenceOffice = ping_presence.isHostOnline("office-pc")
        presenceOffice |= ping_presence.isHostOnline("um560xt")
        publish_presence(mqtt, presenceOffice, "office")

        presenceLiving = ping_presence.isHostOnline('livingroom-pc')
        publish_presence(mqtt, presenceLiving, "livingRoom")

        presenceBedroom = ping_presence.isHostOnline("bedroom-pc")
        publish_presence(mqtt, presenceBedroom, "bedroom")
        Timer(5, update_presence_detection, [mqtt]).start()


mqtt = MqttClient(broker_port=11883, broker_address="mqtt", client_name="PresencefDetector")
master_clock = MasterClock(mqtt, "home/clock")
master_clock.subscribe_to_clock()
update_presence_detection(mqtt)

cache = MqttMultiTopicObservableCache(mqtt, [], "")

mqtt.blocking_run()
