from setuptools import setup, find_packages

setup(
    name='home_auto_lib',
    version='1.0',
    packages=find_packages(),
    install_requires=[
        'paho-mqtt'
    ],
    author='paul',
    author_email='',
)
