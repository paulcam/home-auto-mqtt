import json
import copy
import time
import logging
import math

class JSONObject:
    def __init__(self, data, zone):
        self.data = data
        self.zone = zone

    @classmethod
    def from_json(cls, data_str, zone=None):
        data = json.loads(data_str)
        if not zone and "zone" in data:
            zone = data["zone"]
        if not zone:
            zone = ""
        return cls(data, zone)

    def to_json(self):
        return json.dumps(self.data)

    def __str__(self):
        return "{0!s} {1!s}".format(self.zone, str(self.data))

    def __repr__(self):
        return "{0!s}: {1!s}".format(type(self).__name__, str(self))

    def get_field(self, field):
        return self.data[field] if field in self.data else None

    def set_field(self, field, value):
        self.data[field] = value

    def get_path(self, path, data=None):
        if data is None:
            data = self.data
        current_node = path.partition(".")[0]
        if current_node in data:
            next_node = path.partition(".")[2]
            if next_node == "":
                return data[current_node]
            return self.get_path(next_node, data[current_node])

    def validate(self, time_context=None):
        if not isinstance(self.data, dict):
            logging.error("Object has not .data dict: {0!s}".format(self))
            return False
        if not self.get_field("key"):
            logging.error("Data has no key: {0!s}".format(self.data))
            return False
        if not self.get_field("value"):
            logging.error("No or None value received: {0!s}".format(self.data))
            return False
        return True

    def is_numeric(self):
        if math.isnan(float(self.data["value"])):
            logging.error("NaN value received: {0!s}".format(self.data))
            return False
        return True


class TimestampedJSONObject(JSONObject):
    def __init__(self, data, zone):
        super().__init__(data, zone)

    def get_timestamp(self):
        if "timestamp" in self.data:
            return self.data["timestamp"]
        return None

    def set_timestamp(self, timestamp):
        self.data["timestamp"] = timestamp
        self.data["hr_time"] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(timestamp))

    def is_stale(self, max_age, clock):
        tstamp = self.get_timestamp()
        if not tstamp:
            return True
        if tstamp + max_age < clock:
            return True
        return False

    def validate(self, time_context=None, allow_non_timestamped=True):
        if not super().validate(time_context=time_context):
            return False
        if not self.get_timestamp():
            if allow_non_timestamped:
                self.set_timestamp(time_context)
                logging.warning("Untimestamped Data accepted on key <{0!s}>".format(self.get_field("key")))
            else:
                return False
        return True


class ExpirableData(TimestampedJSONObject):
    def __init__(self, data, zone):
        super().__init__(data, zone)

    def when_expires(self, now):
        if not self.data:
            return -1
        if "timestamp" not in self.data:
            return -1
        if "expiry" not in self.data:
            return -1
        now = float(now)
        then = float(self.get_timestamp())
        how_long = float(self.data["expiry"])
        when = then + how_long
        return when

    def is_expired(self, now):
        when = self.when_expires(now)
        if when == -1:
            return True
        if when >= now:
            return False
        else:
            return True

    def add_metadata(self, key, object):
        if not "metadata" in self.data:
            self.data["metadata"] = dict()
        self.data["metadata"][key]=object

class Demand(ExpirableData):
    def __init__(self, data, zone=None):#
        if "zone" in data:
            zone = data
        super().__init__(data, zone)

    def target(self):
        if "metadata" in self.data:
            data = self.data["metadata"]
            if "target" in data:
                target = data["target"]
                return target
        return None

    def current(self):
        # Demand metadata
        if "metadata" in self.data:
            data = self.data["metadata"]
            if "current" in data:
                current = data["current"]
                return current
        return None

    def schedule(self):
        # Demand metadata
        return self.get_path("metadata.request.metadata.schedule.name", self.data)





    @classmethod
    def from_request(cls, request):
        demand_data = copy.deepcopy(request.data)
        if "metadata" in demand_data:
            metadata = demand_data["metadata"]
            metadata.pop("schedule")
            metadata["request"] = request.data
        else:
            metadata = { "request": request.data}
            demand_data["metadata"] = metadata

        return cls(demand_data, request.zone)

class Request(Demand):
    def __init__(self, data, zone):
        super().__init__(data, zone)


class Control(ExpirableData):
    def __init__(self, data, zone):
        super().__init__(data, zone)


class Presence(ExpirableData):
    def __init__(self, data, zone):
        super().__init__(data, zone)

    def is_present(self):
        if "isPresent" in self.data:
            return bool(self.data["isPresent"])
        else:
            return False
