import time
import sys
import paho.mqtt.client as mqtt
import re
import json
from queue import Queue
from threading import Timer
import logging
from home_auto_lib.random_strings import get_random_string

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)
HEARTBEAT_TIME = 3
SCOUT_TIMEOUT = 10

class MqttClient:
    def __init__(self, broker_address="mqtt", broker_port=1883, client_name="Hub" ):
        self.logger = module_logger.getChild(self.__class__.__name__).getChild(client_name)
        self.logger.addHandler(logging.NullHandler())

        self.is_connected = False
        self.broker_address=broker_address
        self.broker_port= broker_port
        self.client_name = client_name
        self.client = mqtt.Client(client_name) #create new instance
        self.client.on_message=self.on_message        #attach function to callback
        self.client.on_disconnect=self.on_disconnect
        self.client.on_connect=self.on_connect
        self.client.connect(broker_address, broker_port) #connect to broker
        self.subscriptions = set()

    def subscribe(self, topic):
        if topic not in self.subscriptions:
            self.logger.info("New Subscription to {0!s}".format(topic))
            self.client.subscribe(topic)
            self.subscriptions.add(topic)
        else:
            self.logger.debug("Existing Subscription to {0!s}".format(topic))

    def message_callback_add(self, topic, callback):
        self.client.message_callback_add(topic, callback)

    def blocking_run(self):
        self.client.loop_forever()

    def threaded_run(self):
        self.client.loop_start()

    def on_message(self, client, userdata, message):
        pass

    def on_disconnect(self, client, userdata, rc):
        self.is_connected = False
        if rc != 0:
            self.logger.error("Unexpected MQTT disconnection. Will auto-reconnect")

    def on_connect(self, client, userdata, flags, rc):
        self.is_connected = True
        self.logger.info("Got connected to {0!s} on port {1!s}".format(self.broker_address, self.broker_port))
        for sub in self.subscriptions:
            client.subscribe(sub)

    def publish(self, topic, payload, retain=False):
        self.logger.debug("MQTT Attempting to publish {0!s} TOPIC: {1!s}".format(payload, topic))
        self.client.publish(topic,payload, retain=retain)

    def publish_retained(self, topic, payload):
        self.publish(topic, payload, retain=True)


class HAAwareMqttClient(MqttClient):
    def __init__(self, broker_address="mqtt", broker_port=1883, client_name="Hub", service_name=None):
        if not service_name:
            self.service_name=client_name
        else:
            self.service_name=service_name
        self.topic = "ha/service-heartbeats/{0!s}".format(self.service_name)
        client_name = client_name+"-"+get_random_string(4)
        super().__init__( broker_address, broker_port, client_name )

    def subscribe(self, topic):
        if topic not in self.subscriptions:
            self.logger.info("New Subscription to {0!s}".format(topic))
            if self.ha_mode == "active":
                self.client.subscribe(topic)
            else:
                self.logger.info("Subscription added but not activated HA mode not active {0!s}".format(topic))
            self.subscriptions.add(topic)
        else:
            self.logger.debug("Existing Subscription to {0!s}".format(topic))

    def on_disconnect(self, client, userdata, rc):
        self.is_connected = False
        self.ha_mode = "scout"
        self.hb_timer.cancel()
        self.scout_timer.cancel()
        if rc != 0:
            self.logger.error("Unexpected MQTT disconnection. Will auto-reconnect")

    def on_connect(self, client, userdata, flags, rc):
        self.scout_timer = Timer(SCOUT_TIMEOUT, self.active_subscribe)
        self.ha_mode = "scout"
        self.scout_bus()
        self.is_connected = True
        self.hb_timer = Timer(HEARTBEAT_TIME, self.publish_heartbeat)
        self.hb_timer.start()
        self.logger.info("Got connected to {0!s} on port {1!s}".format(self.broker_address, self.broker_port))

    def publish_heartbeat(self):
        if not self.is_connected:
            self.logger.debug("Not sending heartbeat, client not connected")
            return

        if self.ha_mode != "active":
            self.logger.debug("Not sending heartbeat, not active")
            self.hb_timer = Timer(HEARTBEAT_TIME, self.publish_heartbeat)
            self.hb_timer.start()
            return
        self.logger.debug("Sending heartbeat, we are active")
        body = {
            "timestamp": time.time(),
            "client": self.client_name
        }
        self.client.publish(self.topic, json.dumps(body))
        self.hb_timer = Timer(HEARTBEAT_TIME, self.publish_heartbeat)
        self.hb_timer.start()

    def on_heartbeat(self, client, userdata, message):
        if client != self.client:
            return
        payload = json.loads(message.payload)
        try:
            epoch = float(payload["timestamp"])
        except Exception as e:
            self.logger.error("Failed to get timestamp in heartbeat <{0!s}>".format(message.payload))
            sys.exit(1)
        try:
            hb_client = payload["client"]
        except Exception as e:
            self.logger.error("Failed to get client in heartbeat <{0!s}>".format(message.payload))
            sys.exit(1)

        # ACTIVE and healthy
        if hb_client == self.client_name and self.ha_mode == "active":
            self.logger.debug("Healthy heartbeat from myself {0!s}.".format(epoch))
            return

        # NOT ALLOWED
        if hb_client == self.client_name and self.ha_mode == "scout":
            self.logger.error("Heard my own heartbeat in scout mode!  Dying.")
            self.client.loop_stop(force=True)
            sys.exit(1)

        self.logger.debug("Heartbeat for partner {2!s} on {0!s} @ {1!s}".format(message.topic, epoch, hb_client))
        if epoch < time.time()+30:
            self.logger.debug("Valid Heartbeat for partner {2!s} on {0!s} @ {1!s}".format(message.topic, epoch, hb_client))
            self.logger.debug("Remaining in scout mode, restarting timer")
            self.ha_mode = "scout"
            self.active_unsubscribe()
            self.scout_timer.cancel()
            self.scout_timer = Timer(SCOUT_TIMEOUT, self.active_subscribe)
            self.scout_timer.start()
        else:
            self.logger.warning("Not reacting to out of date heartbeat")

    def scout_bus(self):
        self.logger.debug("Starting up in scout mode")

        self.client.subscribe(self.topic)
        self.logger.debug("Subscribed to:{0!s}".format(self.topic))

        self.client.message_callback_add(self.topic, self.on_heartbeat)

        self.logger.debug("Starting scout mode timer")
        self.scout_timer.start()

    def active_subscribe(self):
        self.scout_timer.cancel()
        self.logger.debug("Scout timer timeout, going active")
        self.ha_mode = "active"
        for sub in self.subscriptions:
            self.logger.debug("Activating subscription {0!s}".format(sub))
            self.client.subscribe(sub)

    def active_unsubscribe(self):
        self.logger.debug("Ensuring no active subs.")
        for sub in self.subscriptions:
            self.logger.debug("DEactivating subscription {0!s}".format(sub))
            self.client.unsubscribe(sub)

    def publish(self, topic, payload, retain=False):
        self.logger.debug("self.ha_mode = {0!s}".format(self.ha_mode))
        if self.ha_mode != "active":
            self.logger.warning("Asked to publish with not active topic: {0!s}".format(topic))
            return
        self.logger.debug("MQTT Attempting to publish {0!s} TOPIC: {1!s}".format(payload, topic))
        self.client.publish(topic,payload, retain=retain)

    def publish_retained(self, topic, payload):
        if self.ha_mode != "active":
            return
        self.publish(topic, payload, retain=True)


class MqttTopicCache:
    def __init__(self, client, topic):
        self.logger = module_logger.getChild(self.__class__.__name__).getChild(topic)
        self.logger.addHandler(logging.NullHandler())
        self.last_messages = dict()
        client.subscribe(topic)
        client.message_callback_add(topic, self.on_topic)

    def on_topic(self, client, userdata, message):
        topic = message.topic
        payload = message.payload.decode("UTF-8")
        self.logger.debug("Cache received on {0!s} {1!s}".format(topic, payload))
        self.last_messages[topic] = message.payload.decode("UTF-8")

    def get_last_for_topic(self, topic):
        if topic in self.last_messages:
            return self.last_messages[topic]
        else:
            return None


class MqttMultiTopicObservableCache:

    def __init__(self, client: MqttClient, topic_list, on_change_field):
        self.logger = module_logger.getChild(self.__class__.__name__)
        self.logger.addHandler(logging.NullHandler())
        self.client = client
        self.topic_list = topic_list
        self.cache = dict()
        self.observers = []
        self.subscribe_all()
        self.on_change_field = on_change_field

    def subscribe_all(self):
        for topic in self.topic_list:
            self.logger.info("Subscribing to {0!s}".format(topic))
            self.client.subscribe(topic)
            self.logger.info("Adding message callback for {0!s}".format(topic))
            self.client.message_callback_add(topic, self.on_message)

    def add_observer(self, topic_expression, observer):
        if not (topic_expression, observer) in self.observers:
            self.logger.info("Adding new observer pair {0!s}".format((topic_expression, observer)))
            self.observers.append((topic_expression, observer))
        else:
            self.logger.debug("Ignoring duplicate observer pair {0!s}".format((topic_expression, observer)))

    def on_message(self, client, userdata, message):
        current = message.payload.decode("UTF-8")
        if len(current) < 1:
            return
        try:
            current = json.loads(current)
            if isinstance(current, int) or isinstance(current,float):
                current_value = current
            elif  self.on_change_field and self.on_change_field in current:
                current_value = current[self.on_change_field]
            else:
                current_value = None
        except:
            current_value = None

        # Changed?
        notify = False
        if message.topic in self.cache:
            previous = self.cache[message.topic]
            if isinstance(previous, int) or isinstance(previous,float):
                previous_value = previous
            elif self.on_change_field and self.on_change_field in previous:
                previous_value = previous[self.on_change_field]
            else:
                previous_value = None
            if previous_value != current_value or current_value is None or previous_value is None:
                notify = True
        else:
            notify = True

        self.cache[message.topic] = current
        if notify:
            self.notify_observers(message.topic)

    def notify_observers(self, topic):
        for topic_expression, observer in self.observers:
            # https://codegolf.stackexchange.com/questions/188397/mqtt-subscription-topic-match/188417
            topic_reg = topic_expression.translate({43:"[^/]+",35:".+"})+"$"
            if re.match(topic_reg, topic):
                observer.notify(topic)

    def add_subscription_with_observer(self, topic, observer):
        if topic not in self.topic_list:
            self.add_subscription(topic)
            self.add_observer(topic,observer)

    def add_subscription(self, topic):
        if topic not in self.topic_list:
            self.logger.info("Adding subscription to topic: {0!s}".format(topic))
            self.client.subscribe(topic)
            self.topic_list.append(topic)
            self.client.message_callback_add(topic, self.on_message)

    def last_for_topic(self, topic):
        if topic in self.cache:
            return self.cache[topic]
        return None

    def last_for_topic_expr(self, topic_expr):
        data = dict()
        for key in self.cache.keys():
            if MqttTopicMatcher.matches(key, topic_expr):
                data[key] = self.cache[key]
        return data

class MqttTopicQueue:
    def __init__(self, client, topic):
        self.logger = module_logger.getChild(self.__class__.__name__)
        self.logger.addHandler(logging.NullHandler())
        self.messages=[]
        self.queue = Queue(100)
        client.subscribe(topic)
        client.message_callback_add(topic, self.on_topic)

    def on_topic(self, client, userdata, message):
        topic = message.topic
        payload = message.payload.decode("UTF-8")
        self.logger.debug("Queue received on {0!s} {1!s}".format(topic, payload))
        self.queue.put(message)

    def pop(self):
        self.logger.debug("Waiting on queued item...")
        message = self.queue.get()
        self.logger.debug("Got queued item... {0!s}".format(message))
        return message.topic, message.payload.decode("UTF-8")


class MqttTopicMatcher:
    @classmethod
    def matches(cls, topic, topic_expression):
        topic_reg = topic_expression.translate({43: "[^/]+", 35: ".+"})
        return re.match(topic_reg, topic)
