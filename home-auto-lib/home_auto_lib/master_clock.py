import json
import time
from threading import Thread

class BaseClock:
    instance = None

    def __init__(self):
        if not isinstance(self.instance, BaseClock):
            self.instance = self
        else:
            self = self.instance

        BaseClock.instance = self
        self.clock = int(time.time())
        self.observers = []

    def generate_clock(self):
        clock_thread = Thread(target=self.run)
        clock_thread.start()

    def run(self):
        while True:
            self.publish()
            time.sleep(1)

    def publish(self):
        pass

    def get_current_time(self):
        return self.clock

    def set_current_time(self, current_time):
        self.clock = int(current_time)

    def add_observer(self, observer):
        self.observers.append(observer)

class MasterClock(BaseClock):
    def __init__(self, client, clock_topic):
        super().__init__()
        self.client = client
        self.clock_topic = clock_topic
        self.observers = []

    def publish(self):
        self.client.publish(self.clock_topic, '{{"utctime": {0!s} }}'.format(int(time.time())) )

    def subscribe_to_clock(self):
        self.client.subscribe(self.clock_topic)
        self.client.message_callback_add(self.clock_topic, self.on_message)

    def on_message(self, client, userdata, message ):
        clock_json = message.payload
        clock_obj = json.loads(clock_json)
        clock_val = clock_obj["utctime"]
        self.set_current_time(int(clock_val))
        for ob in self.observers:
            ob(clock_val)

class StaticClock(BaseClock):
    def __init__(self, static_time):
        super().__init__()
        self.set_current_time(static_time)

