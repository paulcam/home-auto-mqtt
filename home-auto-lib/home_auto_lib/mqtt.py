import os
import logging
from home_auto_lib.random_strings import get_random_string
from home_auto_lib.mqtt_lib import MqttClient, MqttMultiTopicObservableCache
from home_auto_lib.master_clock import MasterClock
global SERVICE_NAME
global MQTT_BROKER_ADDR
global MQTT_BROKER_PORT
global MQTT_PROXY_CLIENT_PORT
global client
global internal
global cache
global master_clock
global get_clock

logging.basicConfig(level=logging.DEBUG,
                    format="%(asctime)s %(levelname)s %(message)s [%(filename)s:%(lineno)d]")

if "SERVICE_NAME" in os.environ:
    SERVICE_NAME = os.environ["SERVICE_NAME"]
else:
    SERVICE_NAME = "MqttService-"+get_random_string(4)
    logging.warning("No ENV for SERVICE_NAME defaulting to random {0!s}".format(SERVICE_NAME))

if "MQTT_BROKER_ADDR" in os.environ:
    MQTT_BROKER_ADDR = os.environ["MQTT_BROKER_ADDR"]
else:
    logging.warning("No ENV for MQTT_BROKER_ADDR defaulting to mqtt")
    MQTT_BROKER_ADDR = "10.0.0.74"

MQTT_PROXY_CLIENT_PORT=None
if "proxy" in SERVICE_NAME.lower():
    logging.info("SERVICE_NAME says we are a proxy, creating second client {0!s}".format(SERVICE_NAME))
    if "MQTT_PROXY_CLIENT_PORT" not in os.environ:
        MQTT_PROXY_CLIENT_PORT = 11883
    else:
        MQTT_PROXY_CLIENT_PORT = int(os.environ["MQTT_PROXY_CLIENT_PORT"])

if "MQTT_BROKER_PORT" in os.environ:
    MQTT_BROKER_PORT = int(os.environ["MQTT_BROKER_PORT"])
else:
    if not MQTT_PROXY_CLIENT_PORT:
        logging.warning("No ENV for MQTT_BROKER_PORT defaulting to 1883")
        MQTT_BROKER_PORT = 1883
    else:
        logging.warning("No ENV for MQTT_BROKER_PORT but in PROXY mode so defaulting to 11883")
        MQTT_BROKER_PORT = 1883

logging.info("Config: MQTT_BROKER_ADDR={0!s} MQTT_BROKER_PORT={1!s}".format(MQTT_BROKER_ADDR, MQTT_BROKER_PORT))


if MQTT_PROXY_CLIENT_PORT:
    logging.debug("Connecting... internal")
    internal = MqttClient(broker_address=MQTT_BROKER_ADDR, broker_port=MQTT_BROKER_PORT, client_name=SERVICE_NAME)
    logging.info("Config: PROXY MQTT_BROKER_ADDR={0!s} MQTT_PROXY_CLIENT_PORT={1!s}".format(MQTT_BROKER_ADDR, MQTT_PROXY_CLIENT_PORT))
    logging.debug("Connecting... client")
    client = MqttClient(broker_address=MQTT_BROKER_ADDR, broker_port=MQTT_PROXY_CLIENT_PORT, client_name=SERVICE_NAME)
else:
    logging.debug("Connecting... client only")
    client = MqttClient(broker_address=MQTT_BROKER_ADDR, broker_port=MQTT_BROKER_PORT, client_name=SERVICE_NAME)

cache = MqttMultiTopicObservableCache(client, [], None)

master_clock = MasterClock(client, "home/clock")
master_clock.subscribe_to_clock()


def get_clock():
    return MasterClock.instance.get_current_time()
